/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2021 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include <stdint.h>

#include "lpus.h"

TEST(ST03, EncDec)
{
    int32_t size = 0;
    uint16_t in_sub = 0;
    uint16_t out_sub = 0;
    lpus_struct_t in_struct = { };
    lpus_struct_t out_struct = { };
    uint8_t buf[ST03_REP_PARAMS_NUM_MAX * 12 + 16];

    /* TM[3,25] housekeeping parameters report */
    in_sub = TM0325_HOUSE_PARAM;
    in_struct.st03.tm0325.struct_id = 0xABCD;
    in_struct.st03.tm0325.params_num = ST03_REP_PARAMS_NUM_MAX;
    for (uint8_t i = 0; i < ST03_REP_PARAMS_NUM_MAX; i++) {
        in_struct.st03.tm0325.params[i].id = i;
        in_struct.st03.tm0325.params[i].size = sizeof(uint64_t);
        in_struct.st03.tm0325.params[i].val = i;
    }
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
    size = lpus_decode(buf, size, &out_sub, &out_struct);
    EXPECT_GT(size, 0);
    EXPECT_EQ(in_sub, out_sub);
    EXPECT_EQ(out_struct.st03.tm0325.struct_id, 0xABCD);
    EXPECT_EQ(out_struct.st03.tm0325.params_num, ST03_REP_PARAMS_NUM_MAX);
    for (uint8_t i = 0; i < ST03_REP_PARAMS_NUM_MAX; i++) {
        EXPECT_EQ(in_struct.st03.tm0325.params[i].id, i);
        EXPECT_EQ(in_struct.st03.tm0325.params[i].size, sizeof(uint64_t));
        EXPECT_EQ(in_struct.st03.tm0325.params[i].val, i);
    }
}

TEST(ST03, Invalid)
{
    int32_t size = 0;
    uint16_t in_sub = 0;
    uint16_t out_sub = 0;
    lpus_struct_t in_struct = { };
    lpus_struct_t out_struct = { };
    uint8_t buf[64];

    /* invalid input arguments for TM[3,25] */

    in_sub = TM0325_HOUSE_PARAM;
    in_struct.st03.tm0325.struct_id = 0;
    in_struct.st03.tm0325.params_num = 1;
    in_struct.st03.tm0325.params[0].id = 0x1234;
    in_struct.st03.tm0325.params[0].size = 1;
    in_struct.st03.tm0325.params[0].val = 0x5A;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
    size = lpus_encode(0x0399, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_encode(in_sub, nullptr, buf, sizeof(buf));
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_encode(in_sub, &in_struct, nullptr, sizeof(buf));
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_encode(in_sub, &in_struct, buf, 0);
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
    size = lpus_decode(buf, size, &out_sub, &out_struct);
    EXPECT_GT(size, 0);
    size = lpus_decode(nullptr, size, &out_sub, &out_struct);
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_decode(buf, 0, &out_sub, &out_struct);
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_decode(buf, size, nullptr, &out_struct);
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_decode(buf, size, &out_sub, nullptr);
    EXPECT_EQ(size, (-EINVAL));

    /* invalid parameters inside a report for TM[3,25] */

    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
    in_struct.st03.tm0325.params_num = 16;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, (-EFBIG));
    in_struct.st03.tm0325.params_num = 1;
    in_struct.st03.tm0325.params[0].size = 32;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, (-EINVAL));
}

TEST(ST03, ParamsPad)
{
    int32_t size = 0;
    uint16_t in_sub = 0;
    uint16_t out_sub = 0;
    lpus_struct_t in_struct = { };
    lpus_struct_t out_struct = { };
    uint8_t buf[64];
    uint8_t enc_buf[] = {
        0x20, 0x03, 0x25, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x5, /* 5 params */
        /* params[0], size 8 bytes */
        0x00, 0x00, 0x08,
        0xA8, 0xA7, 0xA6, 0xA5, 0xA4, 0xA3, 0xA2, 0xA1,
        /* params[1], size 4 bytes */
        0x01, 0x00, 0x04,
        0xB4, 0xB3, 0xB2, 0xB1,
        /* params[2], size 2 bytes */
        0x02, 0x00, 0x02,
        0xC2, 0xC1,
        /* params[3], size 1 byte */
        0x03, 0x00, 0x01,
        0xD1,
        /* params[4], size 0 bytes */
        0x04, 0x00, 0x00,
    };

    /* for TM[3,25], if param size is smaller then uint64_t, test padding */

    in_sub = TM0325_HOUSE_PARAM;
    in_struct.st03.tm0325.struct_id = 0;
    in_struct.st03.tm0325.params_num = 3;
    in_struct.st03.tm0325.params[0].id = 1;
    in_struct.st03.tm0325.params[0].size = sizeof(uint64_t);
    in_struct.st03.tm0325.params[0].val = 0x1112131415161718;
    in_struct.st03.tm0325.params[1].id = 2;
    /* params[1] is 8 bytes */
    in_struct.st03.tm0325.params[1].size = sizeof(uint64_t);
    in_struct.st03.tm0325.params[1].val = 0x2122232425262728;
    in_struct.st03.tm0325.params[2].id = 3;
    in_struct.st03.tm0325.params[2].size = sizeof(uint64_t);
    in_struct.st03.tm0325.params[2].val = 0x3132333435363738;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, 51);
    EXPECT_EQ(buf[21], 0x18);
    EXPECT_EQ(buf[28], 0x11);
    EXPECT_EQ(buf[32], 0x28);
    EXPECT_EQ(buf[39], 0x21);
    EXPECT_EQ(buf[43], 0x38);
    EXPECT_EQ(buf[50], 0x31);
    /* params[1] is 4 bytes */
    in_struct.st03.tm0325.params[1].size = sizeof(uint32_t);
    in_struct.st03.tm0325.params[1].val = 0x21222324;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, 47);
    EXPECT_EQ(buf[21], 0x18);
    EXPECT_EQ(buf[28], 0x11);
    EXPECT_EQ(buf[32], 0x24);
    EXPECT_EQ(buf[35], 0x21);
    EXPECT_EQ(buf[39], 0x38);
    EXPECT_EQ(buf[46], 0x31);
    /* params[1] is 2 bytes */
    in_struct.st03.tm0325.params[1].size = sizeof(uint16_t);
    in_struct.st03.tm0325.params[1].val = 0x2122;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, 45);
    EXPECT_EQ(buf[21], 0x18);
    EXPECT_EQ(buf[28], 0x11);
    EXPECT_EQ(buf[32], 0x22);
    EXPECT_EQ(buf[33], 0x21);
    EXPECT_EQ(buf[37], 0x38);
    EXPECT_EQ(buf[44], 0x31);
    /* params[1] is 1 byte */
    in_struct.st03.tm0325.params[1].size = sizeof(uint8_t);
    in_struct.st03.tm0325.params[1].val = 0x21;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, 44);
    EXPECT_EQ(buf[21], 0x18);
    EXPECT_EQ(buf[28], 0x11);
    EXPECT_EQ(buf[32], 0x21);
    EXPECT_EQ(buf[36], 0x38);
    EXPECT_EQ(buf[43], 0x31);
    /* params[1] is 0 bytes */
    in_struct.st03.tm0325.params[1].size = 0;
    in_struct.st03.tm0325.params[1].val = 0x21;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, 43);
    EXPECT_EQ(buf[21], 0x18);
    EXPECT_EQ(buf[28], 0x11);
    EXPECT_EQ(buf[35], 0x38);
    EXPECT_EQ(buf[42], 0x31);
    /* decode message with 5 params of different size */
    size = lpus_decode(enc_buf, sizeof(enc_buf), &out_sub, &out_struct);
    EXPECT_GT(size, 0);
    EXPECT_EQ(out_sub, TM0325_HOUSE_PARAM);
    EXPECT_EQ(out_struct.st03.tm0325.params_num, 5);
    EXPECT_EQ(out_struct.st03.tm0325.params[0].size, sizeof(uint64_t));
    EXPECT_EQ(out_struct.st03.tm0325.params[0].val, 0xA1A2A3A4A5A6A7A8);
    EXPECT_EQ(out_struct.st03.tm0325.params[1].size, sizeof(uint32_t));
    EXPECT_EQ(out_struct.st03.tm0325.params[1].val, 0xB1B2B3B4);
    EXPECT_EQ(out_struct.st03.tm0325.params[2].size, sizeof(uint16_t));
    EXPECT_EQ(out_struct.st03.tm0325.params[2].val, 0xC1C2);
    EXPECT_EQ(out_struct.st03.tm0325.params[3].size, sizeof(uint8_t));
    EXPECT_EQ(out_struct.st03.tm0325.params[3].val, 0xD1);
    EXPECT_EQ(out_struct.st03.tm0325.params[4].size, 0);
    EXPECT_EQ(out_struct.st03.tm0325.params[4].val, 0x00);
}
