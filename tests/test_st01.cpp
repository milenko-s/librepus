/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <gtest/gtest.h>
#include <stdint.h>

#include "lpus.h"
#include "test_data_prust.h"

TEST(ST01, Prust)
{
    const size_t offset = 6; /* offset in Space Packet, were PUS payload begins */
    uint8_t* payload = &prust_st01_packet[offset];
    size_t payload_size = sizeof(prust_st01_packet) - offset;
    uint16_t out_sub = 0;
    lpus_struct_t out_struct = { };

    int32_t size = lpus_decode(payload, payload_size, &out_sub, &out_struct);

    EXPECT_EQ(size, payload_size);
    EXPECT_EQ(out_sub, TM0107_SUCCESS_COMPLETE);
    EXPECT_EQ(0, memcmp(out_struct.st01.tm0107.tc_req_id,
                        prust_st08_packet, 4));
}

TEST(ST01, EncDec)
{
    int32_t size = 0;
    uint16_t in_sub = 0;
    uint16_t out_sub = 0;
    lpus_struct_t in_struct = { };
    lpus_struct_t out_struct = { };
    uint8_t buf[256];
    uint8_t req_id[4] = { 0x89, 0xAB, 0xCD, 0xEF };
    uint8_t fail_data[200] = "oops";
    st01_fail_details_t fail = {
        .code = 0xDEAD,
        .data = fail_data,
        .data_size = sizeof(fail_data),
    };

    /* TM0101_SUCCESS_ACCEPT */
    in_sub = TM0101_SUCCESS_ACCEPT;
    in_struct.st01.tm0101.tc_req_id = req_id;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
    size = lpus_decode(buf, size, &out_sub, &out_struct);
    EXPECT_GT(size, 0);
    EXPECT_EQ(in_sub, out_sub);
    EXPECT_NE(out_struct.st01.tm0101.tc_req_id, nullptr);
    EXPECT_EQ(0, memcmp(in_struct.st01.tm0101.tc_req_id,
                        out_struct.st01.tm0101.tc_req_id, 4));

    /* TM0102_FAILED_ACCEPT */
    in_sub = TM0102_FAILED_ACCEPT;
    in_struct.st01.tm0102.tc_req_id = req_id;
    in_struct.st01.tm0102.fail = fail;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
    size = lpus_decode(buf, size, &out_sub, &out_struct);
    EXPECT_GT(size, 0);
    EXPECT_EQ(in_sub, out_sub);
    EXPECT_NE(out_struct.st01.tm0102.tc_req_id, nullptr);
    EXPECT_EQ(0, memcmp(in_struct.st01.tm0102.tc_req_id,
                        out_struct.st01.tm0102.tc_req_id, 4));
    EXPECT_EQ(in_struct.st01.tm0102.fail.code,
              out_struct.st01.tm0102.fail.code);
    EXPECT_EQ(in_struct.st01.tm0102.fail.data_size,
              out_struct.st01.tm0102.fail.data_size);
    EXPECT_NE(out_struct.st01.tm0102.fail.data, nullptr);
    EXPECT_EQ(0, memcmp(in_struct.st01.tm0102.fail.data,
                        out_struct.st01.tm0102.fail.data,
                        in_struct.st01.tm0102.fail.data_size));

    /* TM0105_SUCCESS_PROGRESS */
    in_sub = TM0105_SUCCESS_PROGRESS;
    in_struct.st01.tm0105.tc_req_id = req_id;
    in_struct.st01.tm0105.step = 0xFF;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
    size = lpus_decode(buf, size, &out_sub, &out_struct);
    EXPECT_GT(size, 0);
    EXPECT_EQ(in_sub, out_sub);
    EXPECT_NE(out_struct.st01.tm0105.tc_req_id, nullptr);
    EXPECT_EQ(0, memcmp(in_struct.st01.tm0105.tc_req_id,
                        out_struct.st01.tm0105.tc_req_id, 4));
    EXPECT_EQ(in_struct.st01.tm0105.step, out_struct.st01.tm0105.step);

    /* other subservices call interaly same funcs, so, no need to test them */
}

TEST(ST01, Invalid)
{
    int32_t size;
    uint16_t in_sub = 0;
    lpus_struct_t in_struct = { };
    uint8_t buf[256];
    uint8_t req_id[4] = { 0x89, 0xAB, 0xCD, 0xEF };
    uint8_t fail_data[200] = "oops";
    st01_fail_details_t fail = {
        .code = 0xDEAD,
        .data = fail_data,
        .data_size = sizeof(fail_data),
    };

    /* invalid inputs for TM[1,1] */

    in_sub = TM0101_SUCCESS_ACCEPT;
    in_struct.st01.tm0101.tc_req_id = req_id;

    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
    size = lpus_encode(0xABCD, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_encode(in_sub, nullptr, buf, sizeof(buf));
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_encode(in_sub, &in_struct, nullptr, sizeof(buf));
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_encode(in_sub, &in_struct, buf, 0);
    EXPECT_EQ(size, (-EINVAL));
    size = lpus_encode(in_sub, &in_struct, buf, 17);
    EXPECT_EQ(size, (-EINVAL));
    in_struct.st01.tm0101.tc_req_id = nullptr;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, (-EINVAL));

    /* extra invalid inputs for TM[1,2] */

    in_sub = TM0102_FAILED_ACCEPT;
    in_struct.st01.tm0102.tc_req_id = req_id;
    in_struct.st01.tm0102.fail = fail;

    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
    in_struct.st01.tm0102.fail.data = nullptr;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
    in_struct.st01.tm0102.fail.data = fail_data;
    in_struct.st01.tm0102.fail.data_size = 444;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_EQ(size, (-EINVAL));
    in_struct.st01.tm0102.fail.data_size = 0;
    size = lpus_encode(in_sub, &in_struct, buf, sizeof(buf));
    EXPECT_GT(size, 0);
}
