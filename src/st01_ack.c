/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "st01_ack_priv.h"

/** Size of Request ID field in TC, to which we reply */
#define TC_REQ_ID_SIZE  4

static int32_t st01_encode_ack(uint8_t* tc_req_id, st01_fail_details_t* fail,
                               step_id_t* step, uint8_t* out_buf,
                               size_t buf_size)
{
    int32_t full_size = TC_REQ_ID_SIZE;
    int32_t cur_size;

    if (step)
        full_size += sizeof(*step);
    if (fail)
        full_size += sizeof(fail->code) + fail->data_size;

    if (!tc_req_id || !out_buf || ((int32_t)buf_size < full_size))
        return (-EINVAL);

    /* relevant to any report */
    memcpy(out_buf, tc_req_id, TC_REQ_ID_SIZE);
    cur_size = TC_REQ_ID_SIZE;

    /* relevant to progress reports */
    if (step) {
        memcpy((out_buf + cur_size), step, sizeof(*step));
        cur_size += sizeof(*step);
    }

    /* relevant to fail reports */
    if (fail) {
        memcpy((out_buf + cur_size), &fail->code, sizeof(fail->code));
        cur_size += sizeof(fail->code);

        if (fail->data) {
            memcpy((out_buf + cur_size), fail->data, fail->data_size);
            cur_size += fail->data_size;
        }
    }

    return cur_size;
}

static int32_t st01_decode_ack(uint8_t* in_buf, size_t data_size,
                               uint8_t** tc_req_id, st01_fail_details_t* fail,
                               step_id_t* step)
{
    int32_t full_size = TC_REQ_ID_SIZE;
    int32_t fail_data_size;
    int32_t cur_size;

    if (step)
        full_size += sizeof(*step);
    if (fail)
        full_size += sizeof(fail->code);

    if (!tc_req_id || !in_buf || ((int32_t)data_size < full_size))
        return (-EINVAL);

    fail_data_size = data_size - full_size;

    /* relevant to any report */
    *tc_req_id = in_buf;
    cur_size = TC_REQ_ID_SIZE;

    /* relevant to progress reports */
    if (step) {
        memcpy(step, (in_buf + cur_size), sizeof(*step));
        cur_size += sizeof(*step);
    }

    /* relevant to fail reports */
    if (fail) {
        memcpy(&fail->code, (in_buf + cur_size), sizeof(fail->code));
        cur_size += sizeof(fail->code);

        fail->data_size = fail_data_size;
        if (fail->data_size)
            fail->data = in_buf + cur_size;
        else
            fail->data = NULL;
        cur_size += fail->data_size;
    }

    return cur_size;
}

bool st01_is_tc(st01_subservice_t in_sub, st01_struct_t* in_struct,
                lpus_tc_ack_t** out_tc_ack)
{
    (void)in_sub;
    (void)in_struct;
    (void)out_tc_ack;

    /* all subservices in ST[01] are TMs */
    return false;
}

int32_t st01_encode(st01_subservice_t in_sub, st01_struct_t* in_struct,
                    uint8_t* out_buf, size_t buf_size)
{
    switch (in_sub) {
        case TM0101_SUCCESS_ACCEPT:
            return st01_encode_ack(in_struct->tm0101.tc_req_id, NULL, NULL,
                                   out_buf, buf_size);

        case TM0102_FAILED_ACCEPT:
            return st01_encode_ack(in_struct->tm0102.tc_req_id,
                                   &in_struct->tm0102.fail, NULL,
                                   out_buf, buf_size);

        case TM0103_SUCCESS_START:
            return st01_encode_ack(in_struct->tm0103.tc_req_id, NULL, NULL,
                                   out_buf, buf_size);

        case TM0104_FAILED_START:
            return st01_encode_ack(in_struct->tm0104.tc_req_id,
                                   &in_struct->tm0104.fail, NULL,
                                   out_buf, buf_size);

        case TM0105_SUCCESS_PROGRESS:
            return st01_encode_ack(in_struct->tm0105.tc_req_id, NULL,
                                   &in_struct->tm0105.step,
                                   out_buf, buf_size);

        case TM0106_FAILED_PROGRESS:
            return st01_encode_ack(in_struct->tm0106.tc_req_id,
                                   &in_struct->tm0106.fail,
                                   &in_struct->tm0106.step,
                                   out_buf, buf_size);

        case TM0107_SUCCESS_COMPLETE:
            return st01_encode_ack(in_struct->tm0107.tc_req_id, NULL, NULL,
                                   out_buf, buf_size);

        case TM0108_FAILED_COMPLETE:
            return st01_encode_ack(in_struct->tm0108.tc_req_id,
                                   &in_struct->tm0108.fail, NULL,
                                   out_buf, buf_size);

        default:
            break;
    }

    return (-EINVAL);
}

int32_t st01_decode(st01_subservice_t in_sub, uint8_t* in_buf,
                    size_t data_size, st01_struct_t* out_struct)
{
    if (!out_struct)
        return (-EINVAL);

    switch (in_sub) {
        case TM0101_SUCCESS_ACCEPT:
            return st01_decode_ack(in_buf, data_size,
                                   &out_struct->tm0101.tc_req_id, NULL, NULL);

        case TM0102_FAILED_ACCEPT:
            return st01_decode_ack(in_buf, data_size,
                                   &out_struct->tm0102.tc_req_id,
                                   &out_struct->tm0102.fail, NULL);

        case TM0103_SUCCESS_START:
            return st01_decode_ack(in_buf, data_size,
                                   &out_struct->tm0103.tc_req_id, NULL, NULL);

        case TM0104_FAILED_START:
            return st01_decode_ack(in_buf, data_size,
                                   &out_struct->tm0104.tc_req_id,
                                   &out_struct->tm0104.fail, NULL);

        case TM0105_SUCCESS_PROGRESS:
            return st01_decode_ack(in_buf, data_size,
                                   &out_struct->tm0105.tc_req_id, NULL,
                                   &out_struct->tm0105.step);

        case TM0106_FAILED_PROGRESS:
            return st01_decode_ack(in_buf, data_size,
                                   &out_struct->tm0106.tc_req_id,
                                   &out_struct->tm0106.fail,
                                   &out_struct->tm0106.step);

        case TM0107_SUCCESS_COMPLETE:
            return st01_decode_ack(in_buf, data_size,
                                   &out_struct->tm0107.tc_req_id, NULL, NULL);

        case TM0108_FAILED_COMPLETE:
            return st01_decode_ack(in_buf, data_size,
                                   &out_struct->tm0108.tc_req_id,
                                   &out_struct->tm0108.fail, NULL);

        default:
            break;
    }

    return (-EINVAL);
}
