/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2021 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "st03_house_priv.h"

static int32_t st03_encode_param(tm03xx_param_rep_t* in_params,
                                 uint8_t* out_buf, size_t buf_size)
{
    int32_t out_size;

    /* first, encode structure_id and params_num */

    /* size of data to write to out_buf */
    out_size = sizeof(in_params->struct_id) + sizeof(in_params->params_num);

    /* data too large for a given out_buf */
    if (out_size > (int32_t)buf_size)
        return (-EFBIG);

    memcpy(out_buf, &in_params->struct_id, sizeof(in_params->struct_id));
    out_buf += sizeof(in_params->struct_id);
    memcpy(out_buf, &in_params->params_num, sizeof(in_params->params_num));
    out_buf += sizeof(in_params->params_num);

    /* second, encode all params in a loop */

    for (uint8_t i = 0; i < in_params->params_num; i++) {
        out_size += sizeof(in_params->params[i].id) +
                    sizeof(in_params->params[i].size) +
                    in_params->params[i].size;

        /* data too large for a given out_buf */
        if (out_size > (int32_t)buf_size)
            return (-EFBIG);

        memcpy(out_buf, &in_params->params[i].id,
               sizeof(in_params->params[i].id));
        out_buf += sizeof(in_params->params[i].id);
        memcpy(out_buf, &in_params->params[i].size,
               sizeof(in_params->params[i].size));
        out_buf += sizeof(in_params->params[i].size);

        /* size of parameter is too big */
        if (in_params->params[i].size > sizeof(in_params->params[i].val))
            return (-EINVAL);

        memcpy(out_buf, &in_params->params[i].val,
               in_params->params[i].size);
        out_buf += in_params->params[i].size;
    }

    return out_size;
}

static int32_t st03_decode_param(uint8_t* in_buf, size_t data_size,
                                 tm03xx_param_rep_t* out_params)
{
    int32_t cur_size;

    /* first, decode structure_id and params_num  */

    cur_size = sizeof(out_params->struct_id) + sizeof(out_params->params_num);

    /* data too small */
    if ((int32_t)data_size < cur_size)
        return (-EINVAL);

    memcpy(&out_params->struct_id, in_buf, sizeof(out_params->struct_id));
    in_buf += sizeof(out_params->struct_id);
    memcpy(&out_params->params_num, in_buf, sizeof(out_params->params_num));
    in_buf += sizeof(out_params->params_num);

    /* second, decode all params in a loop */

    for (uint8_t i = 0; i < out_params->params_num; i++) {
        cur_size += sizeof(out_params->params[i].id) +
                    sizeof(out_params->params[i].size);

        /* data too large for a given out_buf */
        if (cur_size > (int32_t)data_size)
            return (-EFBIG);

        memcpy(&out_params->params[i].id, in_buf,
               sizeof(out_params->params[i].id));
        in_buf += sizeof(out_params->params[i].id);
        memcpy(&out_params->params[i].size, in_buf,
               sizeof(out_params->params[i].size));
        in_buf += sizeof(out_params->params[i].size);

        cur_size += out_params->params[i].size;

        /* data too large for a given out_buf */
        if (cur_size > (int32_t)data_size)
            return (-EFBIG);

        /* size of parameter is too big */
        if (out_params->params[i].size > sizeof(out_params->params[i].val))
            return (-EINVAL);

        memcpy(&out_params->params[i].val, in_buf,
               out_params->params[i].size);
        in_buf += out_params->params[i].size;
    }

    return cur_size;
}

bool st03_is_tc(st03_subservice_t in_sub, st03_struct_t* in_struct,
                lpus_tc_ack_t** out_tc_ack)
{
    bool is_tc = false;
    lpus_tc_ack_t* tc_ack_p = NULL;

    /* TODO: add more subservices, some of which will be TC */
    (void)in_sub;
    (void)in_struct;

    if (out_tc_ack)
        *out_tc_ack = tc_ack_p;

    return is_tc;
}

/* Encode ST[03] housekeeping */
int32_t st03_encode(st03_subservice_t in_sub, st03_struct_t* in_struct,
                    uint8_t* out_buf, size_t buf_size)
{
    switch (in_sub) {
        case TM0325_HOUSE_PARAM:
            return st03_encode_param(&in_struct->tm0325, out_buf, buf_size);

        default:
            break;
    }

    return (-EINVAL);
}

/* Decode ST[03] housekeeping */
int32_t st03_decode(st03_subservice_t in_sub, uint8_t* in_buf,
                    size_t data_size, st03_struct_t* out_struct)
{
    switch (in_sub) {
        case TM0325_HOUSE_PARAM:
            return st03_decode_param(in_buf, data_size, &out_struct->tm0325);

        default:
            break;
    }

    return (-EINVAL);
}
