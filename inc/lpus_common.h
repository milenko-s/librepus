/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Common defines / structs for all service types
 */

#ifndef INC_LPUS_COMMON_H_
#define INC_LPUS_COMMON_H_

#include <stdint.h>

/** TC ACK flags */
typedef union lpus_tc_ack {
    struct {
        uint8_t complete_exec : 1; /**< TC requires ack to completeness of execution */
        uint8_t progress_exec : 1; /**< TC requires ack to progress of execution */
        uint8_t start_exec : 1; /**< TC requires ack to start of execution */
        uint8_t accept_request : 1; /**< TC requires ack to acceptance of request */
        uint8_t unused : 4;
    } flags; /**< individual flags */
    uint8_t value; /**< all flags as a value */
} lpus_tc_ack_t;

#endif /* INC_LPUS_COMMON_H_ */
