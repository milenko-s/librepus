/* Library for ECSS Package Utilization Standard (PUS)
 *
 * Copyright (C) 2020 TU Darmstadt Space Technology e.V.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INC_ST08_FUNC_H_
#define INC_ST08_FUNC_H_

#include <errno.h>
#include <stddef.h>
#include <stdint.h>

#include "lpus_common.h"

/** ST[08] subservices */
typedef enum st08_subservice {
    TC0801_FUNC = 0x0801, /**< TC[8,1] perform function */
} st08_subservice_t;

/** Size of function ID fixed-size string */
#define ST08_FUNC_ID_SIZE   16

/** Structure of function TC[8,1] */
typedef struct tc0801_func {
    lpus_tc_ack_t tc_ack; /**< common ACK flags, oly for TC packets */
    char func_id[ST08_FUNC_ID_SIZE]; /**< function ID */
    uint8_t args_num; /**< number of arguments */
    void* args; /**< pointer to arguments */
    size_t args_size; /**< size of args field */
} tc0801_func_t;

/** Union of all supported ST[08] subservice data structures */
typedef union st08_struct {
    tc0801_func_t tc0801; /**< data structure for TC0801_FUNC */
} st08_struct_t;

#endif /* INC_ST08_FUNC_H_ */
